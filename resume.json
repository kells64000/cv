{
  "$schema": "https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json",
  "basics": {
    "name": "Kélian Bousquet",
    "label": "Développeur",
    "image": "assets/img/me.png",
    "email": "contact@kelianbousquet.com",
    "summary": "Après une quête de plusieurs années, j'ai trouvé ma voie et mon épanouissement dans le domaine du développement d'applications web et mobiles. Grâce à mon parcours atypique, j'ai acquis les compétences nécessaires pour mener à bien des projets de A à Z, de la conception initiale jusqu'au déploiement final. Ce qui me plaît particulièrement dans mon métier, c'est de voir la satisfaction des clients lorsque le produit répond parfaitement à leurs besoins.",
    "location": {
      "city": "Eysines",
      "region": "Nouvelle-Aquitaine",
      "countryCode": "FR"
    },
    "profiles": [
      {
        "network": "LinkedIn",
        "username": "kélian-bousquet",
        "url": "https://www.linkedin.com/in/kélian-bousquet/",
        "icon": "fa-brands fa-linkedin"
      },
      {
        "network": "Gitlab",
        "username": "kells64000",
        "url": "https://gitlab.com/kells64000",
        "icon": "fa-brands fa-gitlab"
      },
      {
        "network": "Github",
        "username": "kells64000",
        "url": "https://github.com/kells64000",
        "icon": "fa-brands fa-github"
      },
      {
        "network": "Symfony",
        "username": "kells",
        "url": "https://connect.symfony.com/profile/kells",
        "icon": "fa-brands fa-symfony"
      },
      {
        "network": "ElePHPant",
        "username": "KelianBousquet",
        "url": "https://elephpant.me/herd/KelianBousquet",
        "icon": "fa-solid fa-republican"
      },
      {
        "network": "Stack-Overflow",
        "username": "kells",
        "url": "https://stackoverflow.com/users/20890528/kells",
        "icon": "fa-brands fa-stack-overflow"
      }
    ]
  },
  "work": [
    {
      "name": "Sensiolabs",
      "position": "Développeur PHP",
      "location": "Asnières-sur-Seine, Île-de-France, France (remote)",
      "url": "https://sensiolabs.com/",
      "logo": "assets/img/logos/Sensiolabs.ico",
      "startDate": "2023-06",
      "summary": "Créateur de Symfony",
      "highlights": [
        "Projet Jobs"
      ]
    },
    {
      "name": "Click&Boat",
      "position": "Développeur PHP",
      "location": "Boulogne-Billancourt, Île-de-France, France (remote)",
      "url": "https://www.clickandboat.com",
      "logo": "assets/img/logos/Click&Boat.ico",
      "startDate": "2022-10",
      "endDate": "2022-12",
      "summary": "Entreprise de location de bateaux",
      "highlights": [
        "Maintenance corrective du site web en PHP 8",
        "Évolution d'un microservice en Symfony 6 avec Api Platform permettant de gérer les périodes de réservations des bateaux",
        "Mise en place de tests e2e avec Playwright"
      ]
    },
    {
      "name": "Thales Services Numériques",
      "position": "Développeur Java | JavaScript",
      "location": "Mérignac, Nouvelle-Aquitaine, France",
      "url": "https://www.thalesgroup.com",
      "logo": "assets/img/logos/Thales.ico",
      "startDate": "2021-09",
      "endDate": "2022-09",
      "summary": "ESN du groupe Thales",
      "highlights": [
        "Création d'une architecture de référentiel en Spring Boot 2 permettant de stocker des valeurs de référence",
        "Migration de projets Spring Boot, Angular et Cordova en version majeure"
      ]
    },
    {
      "name": "ITL Equipment Finance",
      "position": "Développeur PHP | JavaScript",
      "location": "Bordeaux, Nouvelle-Aquitaine, France",
      "url": "https://itlfinance.com",
      "logo": "assets/img/logos/ITL.ico",
      "startDate": "2020-05",
      "endDate": "2021-08",
      "summary": "Entreprise de financement d'équipements à destination des professionnels",
      "highlights": [
        "Maintenance et évolution de l'architecture microservices en Symfony 5",
        "Création d'un référentiel en Symfony 5 avec Api Platform permettant de stocker des produits",
        "Réalisation des écrans consommant le référentiel en Vue 2",
        "Gestion des serveurs sous OVH cloud",
        "Création d'un script de backup des serveurs en bash vers un serveur OVH de stockage"
      ]
    },
    {
      "name": "Scalian",
      "position": "Développeur Java",
      "location": "Le Haillan, Nouvelle-Aquitaine, France",
      "url": "https://www.scalian.com",
      "logo": "assets/img/logos/Scalian.ico",
      "startDate": "2019-10",
      "endDate": "2020-05",
      "summary": "ESN internationale proposant diverses prestations autour du digital",
      "highlights": [
        "Réalisation d'une API en Spring Boot 2 permettant d'effectuer la maintenance et le suivi de pièces aéronautiques",
        "Mise en place de CI/CD via GitLab et Jenkins",
        "Configuration de serveurs destinés à l'intégration et la qualification"
      ]
    },
    {
      "name": "axYus",
      "position": "Développeur PHP | Java",
      "location": "Mérignac, Nouvelle-Aquitaine, France",
      "url": "https://www.axyus.com",
      "logo": "assets/img/logos/axYus.ico",
      "startDate": "2019-01",
      "endDate": "2019-09",
      "summary": "ESN réalisant principalement des applications web métier",
      "highlights": [
        "Maintenance corrective et évolutive d'une application web en PHP 5",
        "Création d'un outil en Java 8 permettant de récupérer l'état d'avancement des tickets dans Mantis Bug Tracker et de les injecter dans ElasticSearch",
        "Réalisation d'une application web en Symfony 4 permettant de récupérer des données de SQUASH TM et d'en extraire un fichier CSV à destination des clients",
        "Mise en place de CI/CD via Jenkins"
      ]
    },
    {
      "name": "TargetWeb",
      "position": "Développeur PHP | JavaScript",
      "location": "Bordeaux, Nouvelle-Aquitaine, France",
      "url": "https://targetweb.fr",
      "logo": "assets/img/logos/TargetWeb.ico",
      "startDate": "2017-11",
      "endDate": "2018-12",
      "summary": "Agence web 360 (design, développement, hébergement et marketing)",
      "highlights": [
        "Intégration de maquettes",
        "Réalisation et maintenance de sites web sous WordPress",
        "Hébergement interne des sites client via Proxmox",
        "Gestion des mails via Postfix",
        "Mise à jour DNS et HTTPS",
        "Création d'un script de backup des serveurs en Python 3 vers AWS S3"
      ]
    },
    {
      "name": "Potez Aéronautique",
      "position": "Technicien systèmes et réseaux",
      "location": "Aire-sur-l'Adour, Nouvelle-Aquitaine, France",
      "url": "https://www.potez.com",
      "logo": "assets/img/logos/Potez.ico",
      "startDate": "2014-09",
      "endDate": "2016-07",
      "summary": "Fabricant d'aérostructures complexes, d'éléments d'aménagement cabine et fournisseur de services industriels",
      "highlights": [
        "Support sur les logiciels bureautique et sur l'ERP de la société",
        "Réparation d'ordinateurs et d'imprimantes",
        "Préparation des nouveaux postes",
        "Configuration des bornes wifi",
        "Administration des serveurs Microsoft et Linux",
        "Réalisation des différents tickets GLPI"
      ]
    }
  ],
  "education": [
    {
      "institution": "Ynov",
      "url": "assets/diplomas/RNCP-DEVLMIOT.pdf",
      "logo": "assets/img/logos/Ynov.ico",
      "area": "Développement logiciel, mobile & IoT",
      "studyType": "RNCP niveau 7",
      "startDate": "2021-10",
      "endDate": "2022-09",
      "courses": [
        "Dart ['Flutter']",
        "JavaScript ['Angular', 'React Native']",
        "Java ['Spring Boot', 'Quarkus']",
        "Database ['MongoDB', 'PostgreSQL']",
        "TDD",
        "BDD"
      ]
    },
    {
      "institution": "CESI",
      "url": "assets/diplomas/RNCP-RILA.pdf",
      "logo": "assets/img/logos/CESI.ico",
      "area": "Responsable en ingénierie des logiciels",
      "studyType": "RNCP niveau 6",
      "startDate": "2017-10",
      "endDate": "2019-09",
      "courses": [
        "Java ['libGDX']",
        "Python |'Django', 'Flask']",
        "Database ['MySQL', 'Oracle']",
        "C#",
        "PHP"
      ]
    },
    {
      "institution": "CESI",
      "url": "assets/diplomas/AP.pdf",
      "logo": "assets/img/logos/CESI.ico",
      "area": "Analyste programmeur",
      "studyType": "RNCP niveau 5",
      "startDate": "2017-01",
      "endDate": "2017-08",
      "courses": [
        "Java ['JavaFX']",
        "Database ['MySQL']",
        "C",
        "PHP",
        "HTML",
        "CSS"
      ]
    },
    {
      "institution": "Greta",
      "url": "assets/diplomas/BTS-SIO.pdf",
      "logo": "assets/img/logos/Greta.ico",
      "area": "Services informatiques aux organisations",
      "studyType": "Brevet de technicien supérieur",
      "startDate": "2014-09",
      "endDate": "2016-09",
      "courses": [
        "VMware ['Workstation', 'ESXi']",
        "Active Directory",
        "Samba 4",
        "DNS",
        "DHCP",
        "IPFire"
      ]
    },
    {
      "institution": "Lycée Guynemer",
      "url": "assets/diplomas/BAC-SEN.pdf",
      "logo": "assets/img/logos/Guynemer.ico",
      "area": "Systèmes Electroniques Numériques",
      "studyType": "Baccalauréat professionnel",
      "startDate": "2012-09",
      "endDate": "2014-07"
    },
    {
      "institution": "Lycée Haute-Vue",
      "url": "assets/diplomas/BAC-RESTAURATION.pdf",
      "logo": "assets/img/logos/Haute-Vue.ico",
      "area": "Restauration",
      "studyType": "Baccalauréat professionnel",
      "startDate": "2009-09",
      "endDate": "2012-07"
    }
  ],
  "certificates": [
    {
      "issuer": "Symfony",
      "url": "assets/certificates/SYMFONY-7.pdf",
      "logo": "assets/img/logos/Symfony.ico",
      "name": "Symfony 7 Developer",
      "date": "2024-12-15"
    },
    {
      "issuer": "Symfony",
      "url": "assets/certificates/SYMFONY-6.pdf",
      "logo": "assets/img/logos/Symfony.ico",
      "name": "Symfony 6 Developer",
      "date": "2023-05-02"
    },
    {
      "issuer": "Cisco",
      "url": "assets/certificates/CISCO-CCNA.pdf",
      "logo": "assets/img/logos/Cisco.ico",
      "name": "CCNA Discovery: Networking for Home and Small Businesses",
      "date": "2013-05-31"
    }
  ],
  "skills": [
    {
      "name": "PHP",
      "keywords": [
        "Symfony",
        "Laravel",
        "WordPress"
      ]
    },
    {
      "name": "Java",
      "keywords": [
        "Spring Boot",
        "Quarkus"
      ]
    },
    {
      "name": "JavaScript",
      "keywords": [
        "Vue"
      ]
    },
    {
      "name": "Typescript",
      "keywords": [
        "Angular",
        "React Native",
        "Ionic"
      ]
    },
    {
      "name": "Dart",
      "keywords": [
        "Flutter"
      ]
    },
    {
      "name": "Python",
      "keywords": [
        "Django",
        "Flask"
      ]
    },
    {
      "name": "Git",
      "keywords": [
        "Gitlab",
        "Github"
      ]
    },
    {
      "name": "Linux",
      "keywords": [
        "Ubuntu",
        "Debian",
        "Centos"
      ]
    },
    {
      "name": "Docker",
      "keywords": [
        "Kubernetes"
      ]
    },
    {
      "name": "Cloud",
      "keywords": [
        "Microsoft Azure",
        "AWS",
        "OVH"
      ]
    }
  ],
  "languages": [
    {
      "language": "Français",
      "fluency": "Langue maternelle",
      "countryCode": "fr"
    },
    {
      "language": "Anglais",
      "fluency": "Professionnel",
      "countryCode": "gb"
    },
    {
      "language": "Espagnol",
      "fluency": "Scolaire",
      "countryCode": "es"
    }
  ],
  "interests": [
    {
      "name": "Cuisine",
      "keywords": [
        "Français",
        "Italien"
      ],
      "icon": "fa-solid fa-utensils"
    },
    {
      "name": "Randonnée",
      "keywords": [
        "Pyrénées",
        "Visorando"
      ],
      "icon": "fa-solid fa-mountain-sun"
    },
    {
      "name": "Meetup",
      "keywords": [
        "AFUP",
        "JUG"
      ],
      "icon": "fa-brands fa-meetup"
    }
  ],
  "meta": {
    "lang": "fr",
    "version": "v1.0.0",
    "canonical": "https://github.com/jsonresume/resume-schema/blob/v1.0.0/schema.json"
  }
}
