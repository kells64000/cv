# CV

🌐 https://cv.kelianbousquet.com

## Install

```bash
docker-compose run --rm node npm install
```

## Generate

```bash
docker-compose run --rm node npm run build
```
